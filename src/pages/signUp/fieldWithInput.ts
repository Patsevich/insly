import { FieldAbstract } from './fieldAbstract'
import { by } from 'protractor'
import { ElementControl } from '../../controls/elementControl'
import { LogHelper } from '../../helpers/logHelper'

export class FieldWithInput extends FieldAbstract {
    get input() {
        return new ElementControl(this.element.element(by.css('input')))
    }

    async fillInput(value: string) {
        await LogHelper.should(`fill '${await this.getId()}' field with '${value}' value`)
        await this.input.fill(value)

        await this.clickOutsideOfInput()

        try {
            await this.waitForIconOk()
        } catch (e) {
            if (await this.iconError.isVisible()) {
                throw new Error(await this.iconError.getText())
            } else {
                throw new Error(e)
            }
        }
    }

    async clickOutsideOfInput() {
        LogHelper.should('should click outside the input')
        await this.label.click()
    }

    async checkElements() {
        const fieldId = await this.getId()

        LogHelper.should(`check the input is visible in '${fieldId}' field`)
        await this.input.assertIsDisplayed()
        await this.checkLabel()
    }
}