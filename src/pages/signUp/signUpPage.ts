import { element, by, browser, ElementArrayFinder } from 'protractor'
import { ElementControl } from '../../controls/elementControl'
import { TermsAndConditionsSection } from './termsAndConditionsSection'
import { configs } from '../../configs/configs'
import { AssertHelper } from '../../helpers/assertHelper'
import { LogHelper } from '../../helpers/logHelper'
import { FieldWithInput } from './fieldWithInput'
import { FieldWithDropdown } from './fieldWithDropdown'
import { PassFieldControl } from './fieldWithPass'

class SignUpPage {
    private _container = element(by.css('.container'))
    private _inslyForm = this._container.element(by.css('div.insly-form'))

    async get() {
        await browser.get(configs.urls.signUp)
        const url = await browser.getCurrentUrl()
        AssertHelper.equal(url, configs.urls.signUp)
    }

    get header() {
        return new ElementControl(this._container.element(by.xpath("//h1[text()='Sign up and start using']")))
    }

    get signUpButton() {
        return new ElementControl(this._inslyForm.element(by.css("button#submit_save")))
    }

    /** Company block fields */
    get companySubtitle() {
        return new ElementControl(this._inslyForm.element(by.xpath("//*[@class='subtitle'][text()='Company']")))
    }

    get companyNameField() {
        return new FieldWithInput(this._inslyForm.element(by.id('field_broker_name')), 'Company name')
    }

    get countryField() {
        return new FieldWithDropdown(this._inslyForm.element(by.id('field_broker_address_country')), 'Country')
    }

    get yourInslyAddressField() {
        return new FieldWithInput(this._inslyForm.element(by.id('field_broker_tag')), 'Your Insly address')
    }

    get companyProfileField() {
        return new FieldWithDropdown(this._inslyForm.element(by.id('field_prop_company_profile')), 'Company Profile')
    }

    get numberOfEmployeesField() {
        return new FieldWithDropdown(this._inslyForm.element(by.id('field_prop_company_no_employees')), 'Number of employees')
    }

    get describeYourselfField() {
        return new FieldWithDropdown(this._inslyForm.element(by.id('field_prop_company_person_description')), 'How would you describe yourself?')
    }

    /** Add User section */
    get addUserSubtitle() {
        return new ElementControl(this._inslyForm.element(by.xpath("//*[@class='subtitle '][text()='Add user']")))
    }

    get addNewBrokerNote() {
        return new ElementControl(this._inslyForm.element(by.css("#field_add_newbroker_note .alert.alert-warning")))
    }

    get inviteButton() {
        return new ElementControl(this._inslyForm.element(by.id("add_newbroker_button")))
    }

    /** Account details section */
    get accountDetailsSubtitle() {
        return new ElementControl(this._inslyForm.element(by.xpath("//*[@class='subtitle'][text()='Administrator account details']")))
    }

    get workEmail() {
        return new FieldWithInput(this._inslyForm.element(by.id('field_broker_admin_email')), 'Work e-mail')
    }

    get accountManagerName() {
        return new FieldWithInput(this._inslyForm.element(by.id('field_broker_admin_name')), 'Account manager name')
    }

    get pass() {
        return new PassFieldControl(element.all(by.id('field_broker_person_password')).first(), 'Password')
    }

    get suggestPass() {
        return new ElementControl(this.pass._elFinder.element(by.xpath("//a[text()='suggest a secure password']")))
    }

    get passRepeat() {
        return new PassFieldControl(element.all(by.id('field_broker_person_password')).last(), 'Password (repeat)')
    }

    get phone() {
        return new FieldWithInput(this._inslyForm.element(by.id('field_broker_admin_phone')), 'Phone')
    }

    /** Terms and conditions fields */
    get termsAndConditionsSubtitle() {
        return new ElementControl(this._inslyForm.element(by.xpath("//*[@class='subtitle'][text()='Terms and conditions']")))
    }

    get termsAndConditions() {
        return new TermsAndConditionsSection(this._inslyForm.element(by.id('field_terms')), 'field_terms')
    }

    get readPolicyNote(): ElementControl {
        return new ElementControl(this._inslyForm.element(by.css("#field_read_privacy_policy_note .alert.alert-warning")))
    }

    /** */


    get loadingMessage() {
        return new ElementControl(element(by.xpath("//p[text()='Wait a little bit while we are building your system and testing your patience.']")))
    }

    getRandomCompanyName() {
        const randomStr = Math.random().toString(36).substr(2, 8)
        LogHelper.log(`Company name - ${randomStr}`)

        return randomStr
    }

    async assertNoVisibleErrors() {
        const elFinderArr: ElementArrayFinder = element.all(by.css("span.icon-error")).filter(async (el) => await el.isDisplayed())
        const count                           = await elFinderArr.count()

        AssertHelper.equal(count, 0, 'There are errors visible on the page')
    }
}

export const signUpPage = new SignUpPage()