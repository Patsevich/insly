import { by, element } from 'protractor'
import { ElementControl } from '../../controls/elementControl'
import { FieldAbstract } from './fieldAbstract'
import { LogHelper } from '../../helpers/logHelper'
import { AssertHelper } from '../../helpers/assertHelper'

export class FieldWithDropdown extends FieldAbstract {
    get dropDown() {
        return new ElementControl(this.element.element(by.css('select')))
    }

    async chooseAnyOptionInDropDown() {
        const dropDownId = await this.dropDown.getId()

        LogHelper.should(`choose any option in '${dropDownId}' dropDown`)
        const allOptions = element.all(by.css(`#${dropDownId} option`))
        await allOptions.get(this.getRandomInt(1, await allOptions.count())).click()

        LogHelper.should("assert the value in dropDown isn't empty")
        AssertHelper.notEqual(await this.dropDown.getAttribute('value'), '')
    }

    getRandomInt(min: number, max: number) {
        min = Math.ceil(min)
        max = Math.floor(max)

        return Math.floor(Math.random() * (max - min)) + min
    }

    async checkElements() {
        const fieldId = await this.getId()

        LogHelper.should(`check the dropDown is visible in '${fieldId}' field`)
        await this.dropDown.assertIsDisplayed()
        await this.checkLabel()
    }
}