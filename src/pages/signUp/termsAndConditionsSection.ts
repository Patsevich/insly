import { element, by, ElementFinder, ElementArrayFinder } from 'protractor'
import { ElementControl } from '../../controls/elementControl'
import { AssertHelper } from '../../helpers/assertHelper'

export class TermsAndConditionsSection extends ElementControl {
    private _fieldId: string

    constructor(el: ElementFinder, id: string) {
        super(el)

        this._fieldId = id
    }

    get termsLink(): ElementControl {
        return new ElementControl(this.getRow(1).element(by.css('a')))
    }

    get privacyLink(): ElementControl {
        return new ElementControl(this.getRow(2).element(by.css('a')))
    }

    getRow(listNumber: 1 | 2 | 3): ElementFinder {
        const allLabels = element.all(by.css(`#${this._fieldId} div.checklist label`))

        return allLabels.get(listNumber - 1)
    }

    getCheckBoxInRow(listNumber: 1 | 2 | 3): ElementControl {
        const label = this.getRow(listNumber)

        return new ElementControl(label.element(by.css(`.icon-check-empty`)))
    }

    async setCheckboxInRow(listNumber: 1 | 2 | 3) {
        const checkbox = this.getCheckBoxInRow(listNumber)

        await checkbox.click()
        AssertHelper.equal(await checkbox.getClass(), 'icon-check-empty icon-check')
    }
}