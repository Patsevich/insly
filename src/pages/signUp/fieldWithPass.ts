import { by, ElementFinder } from 'protractor'
import { ElementControl } from '../../controls/elementControl'
import { FieldWithInput } from './fieldWithInput'

export class PassFieldControl extends FieldWithInput {
    constructor(elFinder: ElementFinder, labelText: string) {
        super(elFinder, labelText)
    }

    get label() {
        return new ElementControl(this._elFinder.element(by.css("div.label")))
    }
}