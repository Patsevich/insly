import { ElementControl } from '../../controls/elementControl'
import { element, ElementFinder } from 'protractor'
import { by } from 'protractor'
import { LogHelper } from '../../helpers/logHelper'
import { AssertHelper } from '../../helpers/assertHelper'

export abstract class FieldAbstract extends ElementControl {
    element: ElementFinder
    private _labelText: string

    constructor(elFinder: ElementFinder, labelText: string) {
        super(elFinder)

        this.element    = this._elFinder.element(by.css('.element'))
        this._labelText = labelText
    }

    get label() {
        return new ElementControl(this._elFinder.element(by.css("span.title")))
    }

    get iconOk() {
        return new ElementControl(this.element.element(by.css("span.icon-ok")))
    }

    get iconError() {
        return new ElementControl(this.element.element(by.css("span.icon-error")))
    }

    async checkLabel() {
        const fieldId = await this.getId()

        LogHelper.should(`check the label is visible in '${fieldId}' field`)
        await this.label.assertIsDisplayed()

        LogHelper.should(`check the '${fieldId}' field has '${this._labelText}' label`)
        const text = await this.label.getText()
        AssertHelper.equal(text.toUpperCase(), this._labelText.toUpperCase())
    }

    async waitForIconOk() {
        await LogHelper.should("wait for 'Icon-ok' is visible")
        await this.iconOk.waitForVisible(this._timeouts.ui.medium)
    }
}