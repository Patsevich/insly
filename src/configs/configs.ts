export const configs = {
    urls : {
        signUp : 'https://signup.int.staging.insly.training/signup',
    },

    timeouts : {
        ui     : {
            small  : 1000,
            medium : 2500,
            large  : 5000,
        },
        server : {
            small  : 3000,
            medium : 10000,
            large  : 20000,
        }
    }
}