export const ptorConfig = {
    framework : 'mocha',

    seleniumAddress : '',

    capabilities : {
        browserName         : 'chrome',
        version             : '*',
        acceptInsecureCerts : true,

        loggingPrefs  : {
            browser : "ALL"
        },
        chromeOptions : {}
    },

    allScriptsTimeout : 30000,

    mochaOpts : {
        reporter        : 'mochawesome-screenshots',
        reporterOptions : {
            reportDir            : '',
            reportTitle          : '',
            reportPageTitle      : '',
            takePassedScreenshot : false,
        },
        timeout         : 300000
    },
}