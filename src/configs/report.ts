const path = require('path')

export const reportConfig = {
    takePassedScreenShot    : false,
    deleteFromCurrentServer : false,

    folderPath : {
        local : path.normalize(__dirname + './../../reports/'), // reports folder path for tests started locally
    },
}