import { LogHelper } from './logHelper'

const _chai = require('chai')
_chai.use(require('chai-smoothie'))

export class AssertHelper {
    static equal<T>(actual: T, expected: T, errMsg?: string): void {
        try {
            _chai.assert.equal(actual, expected, errMsg)
        }
        catch (e) {
            throw new Error(`${errMsg ? errMsg + ". " : ""}elements not equal. Expected:'${expected}'. Actual: '${actual}'.`)
        }
    }

    static notEqual<T>(actual: T, expected: T, errMsg?: string): void {
        try {
            _chai.assert.notEqual(actual, expected, errMsg)
        }
        catch (e) {
            throw new Error(`${errMsg ? errMsg + ". " : ""}values should be not equal. Expected:'${expected}'. Actual: '${actual}'.`)
        }
    }

    static contains(expectedSubstring: string, actualString: string, errMsg?: string, ignoreCase?: boolean): void {
        const defaultMsg = ` String '${actualString}' should contains subString '${expectedSubstring}'${ignoreCase ? '(ignore case)' : ''}.'`
        const msg        = errMsg ? errMsg + defaultMsg : defaultMsg


        if (ignoreCase) {
            expectedSubstring = expectedSubstring.toUpperCase()
            actualString      = actualString.toUpperCase()
        }

        if (!actualString.includes(expectedSubstring))
            throw new Error(msg)
    }

    static isTrue(condition: boolean, errorMessage?: string) {
        if (!condition) {
            throw new Error(
                `${errorMessage}. Expected 'true'; actual '${condition}'`)
        }
    }

    static isFalse(condition: boolean, errorMessage?: string) {
        if (!condition) {
            LogHelper.log(`condition is 'false'. (error did't occur: ${errorMessage})`);
        }
        else
            throw new Error(
                `${errorMessage}. Expected 'false'; actual '${condition}'`)
    }

    /**
     * Asserts valueToCheck is strictly less than (<) valueToBeBelow.*/
    static isBelow(valueToCheck: number, valueToBeBelow: number, errMsg?: string): void {
        try {
            _chai.assert.isBelow(valueToCheck, valueToBeBelow, errMsg)
            LogHelper.log(`value (${valueToCheck}) should be below then (${valueToBeBelow}).(error didn't occur '${errMsg ? errMsg : ""}'`)
        }
        catch (e) {
            throw new Error(`value (${valueToCheck}) should be below then (${valueToBeBelow}). ${errMsg ? errMsg : ""}`)
        }
    }

    /**
     * Asserts valueToCheck is greater than or equal to (>=) valueToBeAtLeast.*/
    static isAtLeast(valueToCheck: number, valueToBeAtLeast: number, errMsg?: string): void {
        try {
            _chai.assert.isAtLeast(valueToCheck, valueToBeAtLeast, errMsg)
            LogHelper.log(`value (${valueToCheck}) should be greater than or equal to (${valueToBeAtLeast}).(error didn't occur '${errMsg ? errMsg : ""}'`)
        }
        catch (e) {
            throw new Error(`value (${valueToCheck}) should be greater than or equal to (${valueToBeAtLeast}). ${errMsg ? errMsg : ""}`)
        }
    }
}