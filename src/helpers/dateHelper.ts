const moment               = require('moment')
const reportFormat: string = 'YYYY_MM_DD__HH_mm_ss'
const simpleFormat: string = 'YYYY/MM/DD HH:mm:ss'
const timeFormat: string   = 'HH:mm:ss.SSS'


export function getFormattedDate(reportsFolder?: true) {
    return reportsFolder
        ? moment(new Date()).format(reportFormat)
        : moment(new Date()).format(simpleFormat)
}

export function getFormattedTime() {
    return moment(new Date()).format(timeFormat)
}