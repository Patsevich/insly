export class TestFlowHelper {
    public static previousFailed = false
    public static currentTest: any

    static skip(test: any) {
        this.currentTest = test
        if (TestFlowHelper.previousFailed) {
            test.skip()
        }
    }
}