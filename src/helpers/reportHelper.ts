import { reportConfig } from "./../configs/report"
import { random } from 'lodash'
import { LogHelper } from './logHelper'
import { getFormattedDate } from './dateHelper'

const path = require('path')

export class ReportHelper {
    private _testConfigFilePath: string

    constructor(testConfigFilePath: string) {
        this._testConfigFilePath = testConfigFilePath
    }

    getFolderPath() {
        const folderName = this.getTestNameWithDate()
        const path       = reportConfig.folderPath.local + folderName

        LogHelper.log(`report folder path is '${path}'`)

        return path
    }

    getTestName(): string {
        const baseName = path.basename(this._testConfigFilePath)

        return baseName.replace('.js', '')
    }

    getTestNameWithDate() {
        return `${this.getTestName()}-${getFormattedDate(true)}_${random(100)}`
    }
}
