import { TestFlowHelper } from './testFlowHelper'
import { getFormattedTime } from './dateHelper'

const chalk     = require('chalk')
const logReport = require('mochawesome-screenshots/logReport')

export class LogHelper {
    static lastMillisecondsStored: number = new Date().valueOf();

    public static log(message: string) {
        const msg = `\t${message}`

        console.log("\t" + chalk.blue(msg))
        if (TestFlowHelper.currentTest != null)
            logReport.log(TestFlowHelper.currentTest, msg)
    }

    public static should(message: string) {
        const msg = `${this.formatMessage('should ' + message)}`

        console.log("\t" + chalk.gray(msg))
        if (TestFlowHelper.currentTest != null)
            logReport.log(TestFlowHelper.currentTest, msg)
    }

    private static formatMessage(message: string): string {
        const currentMilliseconds   = new Date().valueOf()
        const msg                   = `[${getFormattedTime()}] - ${message}`
        this.lastMillisecondsStored = currentMilliseconds

        return msg
    }

}