import { ElementFinder, ExpectedConditions as EC, browser } from 'protractor'
import { configs } from '../configs/configs'
import { AssertHelper } from '../helpers/assertHelper'

const chai = require('chai')
chai.use(require('chai-smoothie'))

export class ElementControl {
    _elFinder: ElementFinder
    protected _timeouts = configs.timeouts

    constructor(elFinder: ElementFinder) {
        this._elFinder = elFinder
    }

    /** Wait*/
    private async _getWaitMsg(msg: string, timeout?: number, parentMsg?: string) {
        const _msg = `The element '${await this.toString()}' ${msg} on the page after ${timeout}ms timeout. `

        return parentMsg ? `${parentMsg}. ${_msg}` : _msg
    }

    async waitForVisible(timeout?: number, errMsg?: string) {
        const _timeout = timeout ? timeout : this._timeouts.ui.medium

        await browser.wait(EC.visibilityOf(this._elFinder), _timeout, await this._getWaitMsg('is not visible', _timeout, errMsg))
    }

    async waitForInvisible(timeout?: number, errMsg?: string) {
        const _timeout = timeout ? timeout : this._timeouts.ui.medium

        await browser.wait(EC.invisibilityOf(this._elFinder), _timeout, await this._getWaitMsg('is still visible', _timeout, errMsg))
    }

    async waitToBeClickable(timeout?: number, errMsg?: string) {
        const _timeout = timeout ? timeout : this._timeouts.ui.medium

        await browser.wait(EC.elementToBeClickable(this._elFinder), _timeout, await this._getWaitMsg('is not clickable', _timeout, errMsg))
    }

    /** Actions */
    async click() {
        await this.waitForVisible()
        await this.waitToBeClickable()
        await this._elFinder.click()
    }

    async fill(value: string) {
        await this.waitForVisible()
        await this._elFinder.sendKeys(value)
    }

    /** Scroll */
    async scrollIntoView() {
        await browser.executeScript("arguments[0].scrollIntoView(false);", this._elFinder.getWebElement())
    }

    /** Get Attributes */
    async getId(): Promise<string> {
        return await this.getAttribute("id")
    }

    async getClass(): Promise<string> {
        return await this._elFinder.getAttribute("class")
    }

    async getAttribute(attributeName: string): Promise<string> {
        return await this._elFinder.getAttribute(attributeName)
    }

    async getText(): Promise<string> {
        return await this._elFinder.getText()
    }

    async toString(): Promise<string> {
        return this._elFinder.locator().toString()
    }

    /** State conditions */
    async isVisible(): Promise<boolean> {
        return await EC.visibilityOf(this._elFinder)()
    }

    async isClickable(): Promise<boolean> {
        return await EC.elementToBeClickable(this._elFinder)()
    }

    /** Assert conditions */
    private async _getAssertMsg(msg: string, parentMsg?: string) {
        const _msg = `Expected the element ${await this.toString()} to be ${msg} but it's not. `

        return parentMsg ? `${parentMsg}. ${_msg}` : _msg
    }

    async assertIsDisplayed(errMsg?: string) {
        AssertHelper.isTrue(await this.isVisible(), await this._getAssertMsg('visible', errMsg))
    }

    async assertIsClickable(errMsg?: string) {
        AssertHelper.isTrue(await this.isClickable(), await this._getAssertMsg('clickable', errMsg))
    }
}