import { ElementControl } from './../elementControl'
import { by, ElementFinder } from 'protractor'

export abstract class PopupAbstract extends ElementControl {
    constructor(elFinder: ElementFinder) {
        super(elFinder)
    }

    get closeIcon() {
        return new ElementControl(this._elFinder.element(by.css('span.icon-close')))
    }

    async close() {
        await this.closeIcon.click()
        await this.waitForInvisible(this._timeouts.ui.small, 'Popup is still visible after click on close')
    }
}