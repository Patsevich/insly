import { ElementControl } from './../elementControl'
import { by, element, ElementFinder } from 'protractor'
import { PopupAbstract } from './popupAbstract'

class TermsAndConditionsPopup extends PopupAbstract {
    constructor(elFinder: ElementFinder) {
        super(elFinder)
    }

    get agreeButton() {
        return new ElementControl(this._elFinder.element(by.css('div.ui-dialog-buttonset button.primary')))
    }
}

class InslyPopup extends PopupAbstract {
    constructor(elFinder: ElementFinder) {
        super(elFinder)
    }

    async getPass(): Promise<string> {
        return await new ElementControl(this._elFinder.element(by.css('div#insly_alert b'))).getText()
    }
}

class PrivacyPolicy extends PopupAbstract {
    constructor(elFinder: ElementFinder) {
        super(elFinder)
    }

    get lastDiv(): ElementControl {
        return new ElementControl(this._elFinder.element(by.xpath("//div[text()='Revision: 1.20180525']")))
    }

    async scrollToTheEnd() {
        await this.lastDiv.scrollIntoView()
    }
}

class OoopsPopup extends PopupAbstract {

}


export const termsAndConditionsPopup = new TermsAndConditionsPopup(element(by.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-dialog-buttons'][.//span[@class='ui-dialog-title'][text()='Terms and conditions']]")))
export const inslyPopup              = new InslyPopup(element(by.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-dialog-buttons'][.//span[@class='ui-dialog-title'][text()='Insly']]")))
export const privacyPolicyPopup      = new PrivacyPolicy(element(by.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable'][.//span[@class='ui-dialog-title'][text()='Privacy Policy']]")))
export const ooopsPopup              = new OoopsPopup(element(by.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-dialog-buttons'][.//span[@class='ui-dialog-title'][text()='Ooops!']]")))

