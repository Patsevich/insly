import { browser, ExpectedConditions as EC } from 'protractor'
import { configs } from '../configs/configs'
import { signUpPage } from '../pages/signUp/signUpPage'
import { AssertHelper } from '../helpers/assertHelper'
import { LogHelper } from '../helpers/logHelper'
import { termsAndConditionsPopup, inslyPopup, privacyPolicyPopup, ooopsPopup } from '../controls/popups/popups'
import { TestFlowHelper } from '../helpers/testFlowHelper'

const testData = {
    accountManagerName : 'John Doe',
    phoneNumber        : '11-11-11',
    url                : '.int.staging.insly.training'
}

describe('SignUp page testing', function () {
    let randomName: string

    before(async function () {
        await browser.waitForAngularEnabled(false)
    })

    beforeEach(async function () {
        TestFlowHelper.currentTest = this
    })

    describe('Step 1', function () {
        it('should open the SignUp page', async function () {
            await signUpPage.get()
        })

        it('should wait for the header', async function () {
            await signUpPage.header.assertIsDisplayed()
        })

        it('should check elements on the page', async function () {
            /** Company section*/
            await signUpPage.companySubtitle.assertIsDisplayed()
            await signUpPage.companyNameField.checkElements()
            await signUpPage.countryField.checkElements()
            await signUpPage.yourInslyAddressField.checkElements()
            await signUpPage.companyProfileField.checkElements()
            await signUpPage.numberOfEmployeesField.checkElements()
            await signUpPage.describeYourselfField.checkElements()

            /** Add user section*/
            await signUpPage.addUserSubtitle.assertIsDisplayed()
            await signUpPage.addNewBrokerNote.assertIsDisplayed()
            await signUpPage.inviteButton.assertIsDisplayed()
            await signUpPage.inviteButton.assertIsClickable()

            /** Account Details section*/
            await signUpPage.accountDetailsSubtitle.assertIsDisplayed()
            await signUpPage.workEmail.checkElements()
            await signUpPage.accountManagerName.checkElements()
            await signUpPage.pass.checkElements()
            await signUpPage.passRepeat.checkElements()
            await signUpPage.phone.checkElements()

            /** Terms And Conditions section*/
            await signUpPage.termsAndConditionsSubtitle.assertIsDisplayed()
            await signUpPage.termsAndConditions.getCheckBoxInRow(1).assertIsDisplayed()
            await signUpPage.termsAndConditions.getCheckBoxInRow(2).assertIsDisplayed()
            await signUpPage.termsAndConditions.getCheckBoxInRow(3).assertIsDisplayed()
            await signUpPage.readPolicyNote.assertIsDisplayed()
        })
    })

    describe('Step 2', function () {
        it('should fill some random unique name in Company name', async function () {
            randomName = signUpPage.getRandomCompanyName()
            await signUpPage.companyNameField.fillInput(randomName)

            LogHelper.should('wait for Insly address is filled automatically')
            while (await signUpPage.yourInslyAddressField.input.getAttribute('value') == '') {
                browser.sleep(500)
            }

            await signUpPage.yourInslyAddressField.waitForIconOk()
        })

        it('should choose any country', async function () {
            await signUpPage.countryField.chooseAnyOptionInDropDown()
        })

        it('should check address is filled automatically', async function () {
            const textInInput = await signUpPage.yourInslyAddressField.input.getAttribute('value')
            AssertHelper.equal(textInInput, randomName)
        })

        it('should select any "Company profile"', async function () {
            await signUpPage.companyProfileField.chooseAnyOptionInDropDown()
        })

        it('should select any "Number of Employees"', async function () {
            await signUpPage.numberOfEmployeesField.chooseAnyOptionInDropDown()
        })

        it('should Select any "HOW WOULD YOU DESCRIBE YOURSELF?"', async function () {
            await signUpPage.describeYourselfField.chooseAnyOptionInDropDown()
        })

        it('should check the address has the following format: {company_name}.int.staging.insly.training', async function () {
            const textInTheField = await signUpPage.yourInslyAddressField.element.getText()
            AssertHelper.equal(randomName + textInTheField, randomName + testData.url)
        })
    })

    describe('Step 3', function () {
        let pass: string

        it('should fill in "Work e-mail"', async function () {
            const email = randomName + '@gmail.com'
            LogHelper.log(`Work email: ${email}`)
            await signUpPage.workEmail.fillInput(email)
        })

        it('should fill in "Account manager name"', async function () {
            await signUpPage.accountManagerName.fillInput(testData.accountManagerName)
        })

        it('should press "suggest a secure password" and remember it', async function () {
            await signUpPage.suggestPass.click()
            await inslyPopup.waitForVisible(configs.timeouts.ui.medium)
            pass = await inslyPopup.getPass()
            LogHelper.log(`pass '${pass}' is saved to a variable`)
            await inslyPopup.close()
        })

        it('should enter a phone number', async function () {
            await signUpPage.phone.fillInput(testData.phoneNumber)
        })
    })

    describe('Step 4', function () {
        it('should tick all terms and conditions', async function () {
            await signUpPage.termsAndConditions.setCheckboxInRow(1)
            await signUpPage.termsAndConditions.setCheckboxInRow(2)
            await signUpPage.termsAndConditions.setCheckboxInRow(3)
        })

        it('should click on "terms and conditions" link and agree', async function () {
            await signUpPage.termsAndConditions.termsLink.click()
            await termsAndConditionsPopup.waitForVisible(configs.timeouts.ui.large)
            await termsAndConditionsPopup.agreeButton.waitForVisible(configs.timeouts.ui.small)
            await termsAndConditionsPopup.agreeButton.click()
            await termsAndConditionsPopup.waitForInvisible(configs.timeouts.ui.small)

        })

        it('should click on "privacy policy" link and scroll down, close popup', async function () {
            await signUpPage.termsAndConditions.privacyLink.click()
            await privacyPolicyPopup.waitForVisible(configs.timeouts.ui.small)
            await privacyPolicyPopup.scrollToTheEnd()
            await privacyPolicyPopup.close()
        })

        it("should check 'Sign up' button is clickable", async function () {
            await signUpPage.signUpButton.assertIsClickable('SignUp button isn not active')
        })
    })

    describe('Step 5', function () {
        let success: boolean

        it("should press 'Sign up' button", async function () {
            await signUpPage.assertNoVisibleErrors()
            await signUpPage.signUpButton.click()
            await browser.wait(EC.or(() => ooopsPopup.isVisible(), () => signUpPage.loadingMessage.isVisible()), configs.timeouts.server.large)

            if (await ooopsPopup.isVisible()) {
                throw new Error("Ooops popup appeared after 'Sign up' button click")
            }

            success = true
        })

        it("should wait for instance creation finish", async function () {
            if (!success)
                this.skip()

            await signUpPage.loadingMessage.waitForInvisible(configs.timeouts.server.large, `loading message hasn't disappeared after ${configs.timeouts.server.large}ms of wait`)

            const fullUrl    = randomName + testData.url
            const currentUrl = await browser.getCurrentUrl()
            const clearedUrl = currentUrl.split('https://')[1]
            await browser.wait(EC.urlIs(randomName + testData.url), configs.timeouts.server.small, `Current url: '${clearedUrl}'. Expected: '${fullUrl}'`)
        })
    })
})