var ReportHelper = require("./dist/helpers/reportHelper")['ReportHelper']
var reportHelper = new ReportHelper(__filename)
var ptorConfig   = require('./dist/configs/ptorConfig')['ptorConfig']


ptorConfig.specs                               = ['./dist/tests/signUp.spec.js']
ptorConfig.mochaOpts.reporterOptions.reportDir = reportHelper.getFolderPath()

exports.config = ptorConfig
